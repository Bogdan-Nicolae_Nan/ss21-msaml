# MsAML-03-1 [[PDF](SS21-MsAML__03-1__Perceptron_convergence.pdf)]

1. [Perceptron convergence](#perceptron-convergence)

Back to [index](index.md).


## Perceptron convergence

After defining the Perceptron model and our first numerical experiments, let
us look more closely at the topic of convergence of the algorithm.

For the case of binary classification, let us make our definition of linear
separability of the training data precise:

Definition
: ##### (Linear separability)
  Two sets $A,B\subset \mathbb R^d$ are called (absolutely) affine-linearly
  separable, if there are $w\in\mathbb R^d,b\in\mathbb R$ such that
  \begin{align}
    \forall x\in A&: w\cdot x+b \geq 0 \qquad (w\cdot x+b > 0),\\
    \text{and }
    \forall x\in B&: w\cdot x+b < 0
  \end{align}
  We say it is linearly separable if we may chose $b=0$.

Note also that we can always represent the left-hand side, being an
affine-linear map, 
\begin{align}
  \underbrace{w}_{\in\mathbb R^d}\cdot \underbrace{x}_{\in\mathbb R^d}
  +\underbrace{b}_{\in\mathbb R} =
  \begin{pmatrix}
    b\\
    w
  \end{pmatrix}
  \cdot
  \begin{pmatrix}
    1\\
    x
  \end{pmatrix}
  =:
  \overline w\cdot\overline x
\end{align}
as the right-hand side, being a linear map on the corresponding
$(d+1)$-dimensional space. Let us use the overline $\overline{\cdot}$ notation
to denote the change in notation. Hence, we will usually drop the word "affine"
when $d$ is not fixed and mention explicitly that the bias term $b$ can be set
to zero if we mean linear instead of affine-linear separability.

If $A,B$ are finite, we immediately get:

Lemma
: ##### (Absolute separability for finite sets)
  For $A,B\subset \mathbb R^d$ with $|A|,|B|<\infty$, linear separability
  implies absolute linear separability.

**Proof:** Suppose $A,B$ are linearly separable. Then there is an $w\in\mathbb
R^d$ and $b\in\mathbb R$ such that
\begin{align}
  \forall \alpha \in A&: w\cdot x+b \geq 0,
  \tag{$\alpha$}
  \label{eq_alpha}
  \\
  \text{and }
  \forall \beta \in B&: w\cdot x+b < 0
  \tag{$\beta$}
  \label{eq_beta}
\end{align}
The idea is to find a minimal distance $\delta>0$, often called margin, between
the data point of $A$ and $B$ and simply underbid it as there is always a
smaller positive real, e.g., $\delta/2$.

![A sketch of the proof.](03/Absolute_separability.png)

Define the minimal distance by
\begin{align}
  \delta := -\max\{w\cdot \beta +b \,|\, \beta\in B\}
\end{align}
which exists as $B$ is finite.

Furthermore, $\delta>0$ because of $\eqref{eq_beta}$.

Define
\begin{align}
  \widetilde b:=b+\delta/2,
\end{align}
which implies for $\alpha\in A$, $\beta\in B$ that
\begin{align}
  w\cdot \alpha+\widetilde b 
  = 
  \underbrace{w\cdot\alpha + b}_{\geq \delta \text{ by }\eqref{eq_alpha}}
  + \underbrace{\delta/2}_{>0} >0 
\end{align}
and likewise
\begin{align}
  w\cdot \beta+\widetilde b 
  = 
  \underbrace{w\cdot\beta + b}_{< -\delta \text{ by }\eqref{eq_beta}}
  + \delta/2 < 0 
\end{align}

Hence, by definition, $A,B$ are absolutely separable with the choice $w,\widetilde b$.

$\square$

Let us now analyze the Perceptron in a simplified setting.

Theorem
: ##### (Perceptron convergence)
  Given training data $s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}$ that is absolutely
  and linearly separable, i.e., the bias terms $b$ can be chosen as zero, the
  Perceptron algorithm for $\eta=1/2$ only performs finite many updates until
  the training data is correctly classified, i.e., until a hypothesis $h_s$ is
  found that fulfills
  \begin{align}
    \forall i=1,\ldots,N: \, y^{(i)}=h_s(x^{(i)}).
  \end{align}

**Proof:** As the training data $s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}$ is
absolutely and linearly separable, we may split it up as follows:
\begin{align}
  A &= \{x^{(i)} \,|\, y^{(i)}=+1, i=1,\ldots, N\}, \\
  B &= \{x^{(i)} \,|\, y^{(i)}=-1, i=1,\ldots, N\}
\end{align}
and know that there is a $w^*\in\mathbb R^d$, i.e., an optimal hyperplane, that
fulfills
\begin{align}
  \forall x \in A&: \, w^*\cdot x > 0,\\
  \forall x \in B&: \, w^*\cdot x < 0.
\end{align}
Furthermore, we observe that these relations feature an undetermined length
scale that we can set to one by normalizing the optimal weight vector $w^*$ and
all feature vectors $x\in A\cup B$; note that we will not encounter a division
by zero. Hence, without loss of generality we may simply assume:
\begin{align}
  |w^*|=1, \qquad \forall i=1,\ldots, N: |x^{(i)}|=1.
\end{align}
The training data can be thought of as projected on the corresponding surface
of the unit sphere.

Say, after the $t$-th execution of the update rule in the Perceptron algorithm,
the current weight vector is given by $w^{(t)}\in\mathbb R^d$ and the
corresponding hypothesis by
\begin{align}
  h^{(t)}(x):=\text{sign}(w^{(t)}\cdot x).
\end{align}

Say, $(x^{(i)},y^{(i)})$ is the next randomly picked feature-label pair, and this leads to a misclassification, i.e.,
\begin{align}
  h^{(t)}(x^{(i)})\neq y^{(i)}.
\end{align}
This condition can equivalently be written as
\begin{align}
  y^{(i)}w^{(t)}\cdot x^{(i)} < 0.
  \tag{misclassification}
  \label{eq_misclassification}
\end{align}

Geometrically, we may depict this situation as follows:

![Training data projected to unit sphere, optimal hyperplane $w^*$, and current hyperplane $w^{(t)}$ after the $t$-th update.](03/Perceptron_convergence.png)

Hence, the update rule would next be executed once more to yield the new weight vector
\begin{align}
  w^{(t+1)} := w^{(t)}+ y^{(i)} x^{(i)},
\end{align}
as in our simplified setting was chosen to $\eta=1/2$.

Geometrically, this change leads to a tilt in our hyperplane from $w^{(t)}$ to
$w^{(t+1)}$. Measuring the angle $\varphi^{(t)}$ of this tilt with with respect
to the unknown optimal hyperplane $w^*$ we get:
\begin{align}
  \cos \varphi^{(t)} = \frac{w^*\cdot w^{(t+1)}}{|w^{(t+1)}|}.
  \tag{cos}
  \label{eq_cos}
\end{align}

The left-hand side of $\eqref{eq_cos}$ is bounded from above by one. If our
intuition about the update rule is correct, the right-hand side should increase
after every update as the hyperplane $w^{(t+1)}$ should have a smaller angle
with hyperplane $w^*$ as compared to hyperplane $w^{(t)}$.

So let us estimate the right-hand side from below without losing the relevant
terms responsible for this increase:

**(1):** We observe that according to the update rule
\begin{align}
  w^*\cdot w^{(t+1)}
  =
  w^*\cdot w^{(t)} + y^{(i)}w^*\cdot x^{(i)}.
\end{align}
As the training data was absolutely and linearly separable we may define
\begin{align}
  \delta := \min\{ |y^{(i)}w^*\cdot x^{(j)}| \,|\, j=1,\ldots,N\},
\end{align}
and note that $\delta > 0$.
Hence, we find
\begin{align}
  w^*\cdot w^{(t+1)} 
  & \geq 
  w^*\cdot w^{(t)} + \delta
  \\
  & \geq w^*\cdot w^{(0)}+ (t+1)\delta
\end{align}
by induction.

**(2):** Regarding the denominator, we compute
\begin{align}
  |w^{(t+1)}|^2 
  = 
  |w^{(t)}|^2 + \underbrace{|y^{(i)}x^{(i)}|^2}_{=1} 
  + \underbrace{2y^{(i)}w^{(t)}\cdot x^{(i)}}_{<0}
\end{align}
as the update rule is only called upon a misclassification; see
$\eqref{eq_misclassification}$. This implies
\begin{align}
  |w^{(t+1)}|^2 
  < |w^{(t)}|^2 + 1
\end{align}
and, again by induction,
\begin{align}
  |w^{(t+1)}|^2 
  < |w^{(0)}|^2 + (t+1).
\end{align}

Plugging **(1)** and **(2)** into $\eqref{eq_cos}$ yields:
\begin{align}
  1 \geq \cos \varphi^{(t)} > 
  \frac{w^*\cdot w^{(0)} + \delta (t+1)}{\sqrt{|w^{(0)}|^2 + (t+1)}}.
\end{align}

The right-hand side of this equation grows as $\sqrt{t+1}$. However, the
left-hand side is bounded from above by one. Hence, only finite many update
are possible before contradicting the bound which concludes the proof.

$\square$

👀
: ##### Homework
  1. We have made several simplification, such as projection to the unit
     sphere, absolute separability, zero bias term, and learning rate
     fixed to $\eta=1/2$ to easily infer a geometric picture. However, it turns
     out that with slight modifications of our bounds we can readily drop all
     of them and obtain a similar result. Generalize our theorem above to
     prove: Given any affine-linearly separable and finite training data, any
     learning rate $\eta>0$, then the Perceptron succeeds to separate the
     training data after only finite many executions of the update rule. In
     this general setting, compute the number of updates in terms of initial
     weights $w^{(0)}=0$, learning rate $\eta>0$, margin bound $\delta>0$, and
     any other relevant features of the training data set. Are those features
     accessible?
  2. The separating hyperplane is not unique. Which one does the Perceptron
     choose? Should the training data not be linearly separable, does the
     Perceptron convergence? Is the algorithm still of use in case of
     "almost"-linear separability of training data?
