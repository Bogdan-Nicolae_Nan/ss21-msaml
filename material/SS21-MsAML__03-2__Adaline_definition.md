# MsAML-03-2 [[PDF](SS21-MsAML__03-2__Adaline_definition.pdf)]

1. [Adaline definition](#adaline-definition)

Back to [index](index.md).


## Adaline definition 

The main drawbacks of the Perceptron algorithm are:

* The update rule stops updating the model parameters $w$ and $b$ as soon as
  linear separability is reached, even if there might be separating
  hyperplanes that have more potential to generalize.
* In case of "almost" linear separability, the update rule causes the model
  parameters to jump back and forth due to the remaining incorrect
  classifications instead of searching for an optimal hyperplane -- e.g., one with the
  lowest number of violations. 

It will turn out that a minor modification allows to address these shortcomings
to some extend. In particular, it will allow to specify better a notion of
optimality in a way that allows to apply regular optimization theory.

In the Perceptron algorithm, training data feeds back information to the update
rule as follows:

![Illustration of the Perceptron feedback loop.](03/Perceptron_feedback_loop.png)

This illustration depicts the various steps carried out during an update. The
feature signals in form of the feature vector $x\in\mathbb R^d$ are fed into
the weighted sum $w\cdot x$. The produced "signal" of the artificial neuron is
then lifted by means of the bias term $b$ to result in the activation signal
$w\cdot x+b$. Afterwards, it is "cleaned" to enforce an output signal
$y\in\{-1,+1\}$. In case of a misclassification, it is the cleaned, and
therefore discrete, output signal that is fed back to the update rule in order
to adjust the weights in $w$ and the bias $b$.

Next, we will implement a tiny adaptation of this feedback loop that will turn
out to have a quite big effect:

![Illustration of the Adaline feedback loop.](03/Adaline_feedback_loop.png)

This architecture is called the *ada*ptive *line*ar neuron, or Adaline
neuron for short. The main difference is that we will change the algorithm to
continuously feed information about the activation signal back before making
the signal discrete by means of the $\text{sign}$ function. 

The activation signal will be computed from $w\cdot x+b$ by applying another
function
\begin{align}
  \alpha:\mathbb R\to\mathbb R,
\end{align}
the so-called *activation function* whose value we will call *activation
output*. A sufficiently regular choice in this function will be the first
ingredient to apply regular optimization theory.

Note that the activation input $w\cdot x+b$ is unbounded, which could render a
comparison to $y^{(i)}$ ill-posed as the activation input and the actual labels
live on different scales. However, we could simulate the discrete $\text{sign}$
function by a smooth version of it. For example for $\mathcal Y=\{-1,+1\}$, we
could choose the so-called *sigmoid* function
\begin{align}
  \alpha(z):=\tanh(z).
\end{align}
Now $y{(i)}$ and $h^\alpha_{(w,b)}$ are again on the "same" scale, and
furthermore, $h^\alpha_{(w,b)}$ is as smooth as $\alpha$.

Next, let us introduce a loss function to formalize the comparison between
actual labels $y^{(i)}$ and activation output $h^\alpha_{(w,b)}$. As for
$\alpha$, we will discuss various choices in the coming modules, but for now
let us assume that our label space $\mathcal Y$ comprises only the numbers $-1$
and $+1$ and start with the simple choice:
\begin{align}
  L: \mathcal Y &\to\mathcal Y
  \\
  (y,y') &\mapsto \frac12|y-y'|^2,
\end{align}
which for training data point $(x^{(i)},y^{(i)})$ equals half the square
distance between the actual class label $y^{(i)}$. The factor $\frac12$ is only
kept for convenience as we will mainly be working with the corresponding derivative.
This allows to define the loss between actual label $y^{(i)}$ and activation
output as response to input $x^{(i)}$ by
\begin{align}
  L\left(y^{(i)},h^\alpha_{(w,b)}(x^{(i)})\right),
  \qquad
  h^\alpha_{(w,b)}(x^{(i)}) := \alpha(w\cdot x+b).
\end{align}
Furthermore, we accumulate these losses and define the empirical risk of the
activation output hypothesis $h^\alpha_{(w,b)}$ as
\begin{align}
  \widehat R(h^\alpha_{(w,b)})=\frac{1}{N}\sum_{i=1}^N L\left(y^{(i)},h^\alpha_{(w,b)}(x^{(i)})\right).
\end{align}
which, to emphasize it once more, is as smooth as $\alpha$ and $L$.

Remark.
: Note however that the hypothesis used for assigning the predicted class labels
  is not given by the activation output $h^\alpha_{(w,b)}$ but by the "cleaned"
  hypothesis 
  \begin{align}
    h_{(w,b)}(x) := \text{sign}(h^\alpha_{(w,b)}(x))
  \end{align}
  that projects the activation output to the discreet label space $\mathcal Y$.
  Hence, although closely related, the empirical risk of the activation output
  $h^\alpha_{(w,b)}$ will differ from the empirical risk of the classifier 
  $h_{(w,b)}$
  \begin{align}
    \widehat R(h_{(w,b)})=\frac{1}{N}\sum_{i=1}^N L\left(y^{(i)},h_{(w,b)}(x^{(i)})\right),
  \end{align}
  and likewise, of the error probability
  \begin{align}
    \widehat{\text{Err}}(h_{(w,b)})=\frac{1}{N}\sum_{i=1}^N 1_{y^{(i)}\neq h_{(w,b)}(x^{(i)})}.
  \end{align}
 
It is the goal, of course, to minimize the empirical risk $\widehat
R(h_{(w,b)})$. However, due to the $\text{sign}$ function, the latter is not
continuous, not to mention differentiable, no matter how smooth $\alpha$ or $L$
was chosen; and the same holds for the empirical error. This makes it
impossible to apply regular optimization methods. Therefore, instead of
attempting to minimize the empirical risk directly we will focus on the closely
related empirical risk of the activation output for sufficiently smooth
$\alpha$ and $L$.

The introduction of $\alpha$ and $L$ will help us to quip the model with a
mathematically precise notion of priori knowledge about:

* How close the model is to make an accurate classification of the training data;
* and how optimal a potential hypothesis is.

Especially, for the latter, $L$ has to be chosen with much care.

To keep our notation short, let us introduce the abbreviation
\begin{align}
  \widehat L(w,b) = \widehat R(h^\alpha_{(w,b)})
\end{align}
and let us for a moment assume that the model parameters $w$ and $b$ are
already sufficiently close to a local minimum of $\widehat
R(h^\alpha_{(w,b)})$.

How should the model parameters $w$ and $b$ be updated 
\begin{align}
  w & \leftarrowtail w^\text{new} := w + \delta w \\
  b & \leftarrowtail b^\text{new} := b + \delta b
\end{align}
to get even closer to the minimum, i.e., such that 
\begin{align}
  \widehat L(w^\text{new})\leq \widehat L(w)
  \tag{min}
  \label{eq_min}
\end{align}
is fulfilled?

Let us consider a heuristic argument and employ the notation $\overline w=(w,b)$:
\begin{align}
  \widehat L(\overline w^\text{new}) - \widehat L(\overline w)
  &=
  \widehat L(\overline w+\delta \overline w) - \widehat L(\overline w)
  \\
  &=
  \frac{\partial \widehat L(\overline w)}{\partial \overline w} \delta\overline w
  +
  O_{|\delta \overline w|\to 0}(|\delta \overline w|^2),
\end{align}
where $\frac{\partial}{\partial\overline w}$ denotes the gradient in $\mathbb
R^{d+1}$ with respect to $w\in\mathbb R^d$ and $b\in\mathbb R$.  Hence, choosing
\begin{align}
  \delta\overline w = -\eta \frac{\partial \widehat L(\overline w)}{\partial \overline w}
  \tag{negative gradient}
  \label{eq_negative_gradient}
\end{align}
for a learning rate $\eta>0$, results in 
\begin{align}
  \widehat L(\overline w^\text{new}) - \widehat L(\overline w)
  &=
  \widehat L(\overline w+\delta \overline w) - \widehat L(\overline w)
  \\
  &=
  -\eta
  \left|\frac{\partial \widehat L(\overline w)}{\partial \overline w} w\right|^2
  +
  O_{\eta \to 0}(\eta^2).
\end{align}
Note how our choice $\eqref{eq_negative_gradient}$ in taking the negative
gradient of $\widehat L$ at $\overline w$ fixes the sign of the linear order
term.  In case the higher order corrections, whose modulus can be estimated by
means of the Taylor remainder, in our case, involving the Hesse matrix of
$\widehat L(\overline w)$, does not outweigh this negativity of first order term,
the new parameters $\overline w+\delta\overline w$ brought us even closer to
the minimum, i.e., $\eqref{eq_min}$. This is the core idea behind a procedure
that is referred to as *gradient descent*, owing its name to
$\eqref{eq_negative_gradient}$.

👀
: Is there a bound on the higher orders which is accessible in applications?

Of course, this heuristic discussion only scratches the surface of search
policies to find a minimum of $\widehat L$ in regular optimization theory.
Under which conditions do we converge? And if we do, to which local minimum?
And will this local minimum be a global one? What to do to not get stuck in
local minima that still feature a high value of $\widehat L$? And there are
many more sophisticated algorithms, like the Newton's method you might recall
from your analysis lectures to mention a simple one. Nevertheless, it will turn
out that, for application, the gradient descent will perform reasonably well
without relying on second order derivatives that are very expensive to compute
once the model parameters become large.

In summary, we may formalize the Adaline update rule as follows:

**Update rule:** The component-wise Adaline update rule is given by
\begin{align}
  w & \leftarrowtail w^\text{new}_j := w_j -\eta \frac{\partial \widehat L(w,b)}{\partial w_j} \\
  b & \leftarrowtail b^\text{new} := b -  \eta \frac{\partial \widehat L(w,b)}{\partial b}
\end{align}
for components $j=1,\ldots,d$.

Furthermore, we specify the first version of the Adaline algorithm by:

Definition 
: ##### (Adaline)
  The realization (as it is actually a random variable when defined as below) of the Adaline 
  algorithm is a map 
  \begin{align}
    \mathcal A:\cup_{N\in\mathbb N} (\mathbb R^d,\{-1,1\})^N\to \mathcal H
  \end{align}
  which is specified by means of the computation steps below:

**Input for $\mathcal A$:** Training data
\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N}.
\end{align}

**Step 1:** Initialize $w\in\mathbb R^d$ and $b\in \mathbb R$ with random
coefficients which defines the activation output
\begin{align}
  h^\alpha_{(w,b)}(x) := \alpha(w\cdot x +b).
\end{align}
and hypothesis
\begin{align}
  h_{(w,b)}(x) := \text{sign}(w\cdot x +b).
\end{align}

**Step 2:** Until a maximum total number of repetitions is reached:

  1. Compute the empirical activation loss $\widehat L(w,b)$.
  2. Conduct the **Update rule** above to infer the parameter updates:
    \begin{align}
      w & \leftarrowtail w^\text{new}\\
      b & \leftarrowtail b^\text{new}
    \end{align}

and return to **Step 2**.

**Output of $\mathcal A$:** The adjusted parameters $w\in\mathbb R^d$ and
$b\in\mathbb R$ and therefore a "hopefully" better hypothesis
\begin{align}
  h_{(w,b)} \in\mathcal H.
\end{align}

Each iteration in the above algorithm is usually called a *training epoch*. In
the next module we will compare the Perceptron and Adaline algorithms a bit
closer and discuss some useful variations.

➢ Next session!


👀 Homework.
: 1. Prove that, even for linearly separable data, the Adaline algorithm for
     constant $\eta>0$ may not converge, no many how many epochs we allow. This will
     even typically be the case. Argue why the Adaline may nevertheless behave
     better than the Perceptron in view of our initial motivation above.
  2. Consider a linear activation function $\alpha(z)=z$ and the square loss
     $L(y,y')=\frac12|y-y'|^2$ as above. Given training data
     $s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}$, does a global minimum of $\widehat
     L(w,b)$ exist? Can we enforce convergence by allowing the Adaline
     algorithm, e.g., to decrease $\eta$, after each epoch of training? Discuss
     the trade-off between speed of convergence and accuracy for smaller
     $\eta$. 
