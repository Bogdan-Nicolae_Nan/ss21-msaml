<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__03-1__Perceptron_convergence</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-03-1-pdf">MsAML-03-1 [<a href="SS21-MsAML__03-1__Perceptron_convergence.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#perceptron-convergence">Perceptron convergence</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="perceptron-convergence">Perceptron convergence</h2>
<p>After defining the Perceptron model and our first numerical experiments, let us look more closely at the topic of convergence of the algorithm.</p>
<p>For the case of binary classification, let us make our definition of linear separability of the training data precise:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="linear-separability">(Linear separability)</h5>
Two sets <span class="math inline">\(A,B\subset \mathbb R^d\)</span> are called (absolutely) affine-linearly separable, if there are <span class="math inline">\(w\in\mathbb R^d,b\in\mathbb R\)</span> such that <span class="math display">\[\begin{align}
\forall x\in A&amp;: w\cdot x+b \geq 0 \qquad (w\cdot x+b &gt; 0),\\
\text{and }
\forall x\in B&amp;: w\cdot x+b &lt; 0
  \end{align}\]</span> We say it is linearly separable if we may chose <span class="math inline">\(b=0\)</span>.
</dd>
</dl>
<p>Note also that we can always represent the left-hand side, being an affine-linear map, <span class="math display">\[\begin{align}
  \underbrace{w}_{\in\mathbb R^d}\cdot \underbrace{x}_{\in\mathbb R^d}
  +\underbrace{b}_{\in\mathbb R} =
  \begin{pmatrix}
    b\\
    w
  \end{pmatrix}
  \cdot
  \begin{pmatrix}
    1\\
    x
  \end{pmatrix}
  =:
  \overline w\cdot\overline x
\end{align}\]</span> as the right-hand side, being a linear map on the corresponding <span class="math inline">\((d+1)\)</span>-dimensional space. Let us use the overline <span class="math inline">\(\overline{\cdot}\)</span> notation to denote the change in notation. Hence, we will usually drop the word “affine” when <span class="math inline">\(d\)</span> is not fixed and mention explicitly that the bias term <span class="math inline">\(b\)</span> can be set to zero if we mean linear instead of affine-linear separability.</p>
<p>If <span class="math inline">\(A,B\)</span> are finite, we immediately get:</p>
<dl>
<dt>Lemma</dt>
<dd><h5 id="absolute-separability-for-finite-sets">(Absolute separability for finite sets)</h5>
For <span class="math inline">\(A,B\subset \mathbb R^d\)</span> with <span class="math inline">\(|A|,|B|&lt;\infty\)</span>, linear separability implies absolute linear separability.
</dd>
</dl>
<p><strong>Proof:</strong> Suppose <span class="math inline">\(A,B\)</span> are linearly separable. Then there is an <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in\mathbb R\)</span> such that <span class="math display">\[\begin{align}
  \forall \alpha \in A&amp;: w\cdot x+b \geq 0,
  \tag{$\alpha$}
  \label{eq_alpha}
  \\
  \text{and }
  \forall \beta \in B&amp;: w\cdot x+b &lt; 0
  \tag{$\beta$}
  \label{eq_beta}
\end{align}\]</span> The idea is to find a minimal distance <span class="math inline">\(\delta&gt;0\)</span>, often called margin, between the data point of <span class="math inline">\(A\)</span> and <span class="math inline">\(B\)</span> and simply underbid it as there is always a smaller positive real, e.g., <span class="math inline">\(\delta/2\)</span>.</p>
<figure>
<img src="03/Absolute_separability.png" alt="A sketch of the proof." /><figcaption aria-hidden="true">A sketch of the proof.</figcaption>
</figure>
<p>Define the minimal distance by <span class="math display">\[\begin{align}
  \delta := -\max\{w\cdot \beta +b \,|\, \beta\in B\}
\end{align}\]</span> which exists as <span class="math inline">\(B\)</span> is finite.</p>
<p>Furthermore, <span class="math inline">\(\delta&gt;0\)</span> because of <span class="math inline">\(\eqref{eq_beta}\)</span>.</p>
<p>Define <span class="math display">\[\begin{align}
  \widetilde b:=b+\delta/2,
\end{align}\]</span> which implies for <span class="math inline">\(\alpha\in A\)</span>, <span class="math inline">\(\beta\in B\)</span> that <span class="math display">\[\begin{align}
  w\cdot \alpha+\widetilde b 
  = 
  \underbrace{w\cdot\alpha + b}_{\geq \delta \text{ by }\eqref{eq_alpha}}
  + \underbrace{\delta/2}_{&gt;0} &gt;0 
\end{align}\]</span> and likewise <span class="math display">\[\begin{align}
  w\cdot \beta+\widetilde b 
  = 
  \underbrace{w\cdot\beta + b}_{&lt; -\delta \text{ by }\eqref{eq_beta}}
  + \delta/2 &lt; 0 
\end{align}\]</span></p>
<p>Hence, by definition, <span class="math inline">\(A,B\)</span> are absolutely separable with the choice <span class="math inline">\(w,\widetilde b\)</span>.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>Let us now analyze the Perceptron in a simplified setting.</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="perceptron-convergence-1">(Perceptron convergence)</h5>
Given training data <span class="math inline">\(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\)</span> that is absolutely and linearly separable, i.e., the bias terms <span class="math inline">\(b\)</span> can be chosen as zero, the Perceptron algorithm for <span class="math inline">\(\eta=1/2\)</span> only performs finite many updates until the training data is correctly classified, i.e., until a hypothesis <span class="math inline">\(h_s\)</span> is found that fulfills <span class="math display">\[\begin{align}
\forall i=1,\ldots,N: \, y^{(i)}=h_s(x^{(i)}).
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> As the training data <span class="math inline">\(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\)</span> is absolutely and linearly separable, we may split it up as follows: <span class="math display">\[\begin{align}
  A &amp;= \{x^{(i)} \,|\, y^{(i)}=+1, i=1,\ldots, N\}, \\
  B &amp;= \{x^{(i)} \,|\, y^{(i)}=-1, i=1,\ldots, N\}
\end{align}\]</span> and know that there is a <span class="math inline">\(w^*\in\mathbb R^d\)</span>, i.e., an optimal hyperplane, that fulfills <span class="math display">\[\begin{align}
  \forall x \in A&amp;: \, w^*\cdot x &gt; 0,\\
  \forall x \in B&amp;: \, w^*\cdot x &lt; 0.
\end{align}\]</span> Furthermore, we observe that these relations feature an undetermined length scale that we can set to one by normalizing the optimal weight vector <span class="math inline">\(w^*\)</span> and all feature vectors <span class="math inline">\(x\in A\cup B\)</span>; note that we will not encounter a division by zero. Hence, without loss of generality we may simply assume: <span class="math display">\[\begin{align}
  |w^*|=1, \qquad \forall i=1,\ldots, N: |x^{(i)}|=1.
\end{align}\]</span> The training data can be thought of as projected on the corresponding surface of the unit sphere.</p>
<p>Say, after the <span class="math inline">\(t\)</span>-th execution of the update rule in the Perceptron algorithm, the current weight vector is given by <span class="math inline">\(w^{(t)}\in\mathbb R^d\)</span> and the corresponding hypothesis by <span class="math display">\[\begin{align}
  h^{(t)}(x):=\text{sign}(w^{(t)}\cdot x).
\end{align}\]</span></p>
<p>Say, <span class="math inline">\((x^{(i)},y^{(i)})\)</span> is the next randomly picked feature-label pair, and this leads to a misclassification, i.e., <span class="math display">\[\begin{align}
  h^{(t)}(x^{(i)})\neq y^{(i)}.
\end{align}\]</span> This condition can equivalently be written as <span class="math display">\[\begin{align}
  y^{(i)}w^{(t)}\cdot x^{(i)} &lt; 0.
  \tag{misclassification}
  \label{eq_misclassification}
\end{align}\]</span></p>
<p>Geometrically, we may depict this situation as follows:</p>
<figure>
<img src="03/Perceptron_convergence.png" alt="Training data projected to unit sphere, optimal hyperplane w^*, and current hyperplane w^{(t)} after the t-th update." /><figcaption aria-hidden="true">Training data projected to unit sphere, optimal hyperplane <span class="math inline">\(w^*\)</span>, and current hyperplane <span class="math inline">\(w^{(t)}\)</span> after the <span class="math inline">\(t\)</span>-th update.</figcaption>
</figure>
<p>Hence, the update rule would next be executed once more to yield the new weight vector <span class="math display">\[\begin{align}
  w^{(t+1)} := w^{(t)}+ y^{(i)} x^{(i)},
\end{align}\]</span> as in our simplified setting was chosen to <span class="math inline">\(\eta=1/2\)</span>.</p>
<p>Geometrically, this change leads to a tilt in our hyperplane from <span class="math inline">\(w^{(t)}\)</span> to <span class="math inline">\(w^{(t+1)}\)</span>. Measuring the angle <span class="math inline">\(\varphi^{(t)}\)</span> of this tilt with with respect to the unknown optimal hyperplane <span class="math inline">\(w^*\)</span> we get: <span class="math display">\[\begin{align}
  \cos \varphi^{(t)} = \frac{w^*\cdot w^{(t+1)}}{|w^{(t+1)}|}.
  \tag{cos}
  \label{eq_cos}
\end{align}\]</span></p>
<p>The left-hand side of <span class="math inline">\(\eqref{eq_cos}\)</span> is bounded from above by one. If our intuition about the update rule is correct, the right-hand side should increase after every update as the hyperplane <span class="math inline">\(w^{(t+1)}\)</span> should have a smaller angle with hyperplane <span class="math inline">\(w^*\)</span> as compared to hyperplane <span class="math inline">\(w^{(t)}\)</span>.</p>
<p>So let us estimate the right-hand side from below without losing the relevant terms responsible for this increase:</p>
<p><strong>(1):</strong> We observe that according to the update rule <span class="math display">\[\begin{align}
  w^*\cdot w^{(t+1)}
  =
  w^*\cdot w^{(t)} + y^{(i)}w^*\cdot x^{(i)}.
\end{align}\]</span> As the training data was absolutely and linearly separable we may define <span class="math display">\[\begin{align}
  \delta := \min\{ y^{(i)}w^*\cdot x^{(j)} \,|\, j=1,\ldots,N\},
\end{align}\]</span> and note that <span class="math inline">\(\delta &gt; 0\)</span>. Hence, we find <span class="math display">\[\begin{align}
  w^*\cdot w^{(t+1)} 
  &amp; \geq 
  w^*\cdot w^{(t)} + \delta
  \\
  &amp; \geq w^*\cdot w^{(0)}+ (t+1)\delta
\end{align}\]</span> by induction.</p>
<p><strong>(2):</strong> Regarding the denominator, we compute <span class="math display">\[\begin{align}
  |w^{(t+1)}|^2 
  = 
  |w^{(t)}|^2 + \underbrace{|y^{(i)}x^{(i)}|^2}_{=1} 
  + \underbrace{2y^{(i)}w^{(t)}\cdot x^{(i)}}_{&lt;0}
\end{align}\]</span> as the update rule is only called upon a misclassification; see <span class="math inline">\(\eqref{eq_misclassification}\)</span>. This implies <span class="math display">\[\begin{align}
  |w^{(t+1)}|^2 
  &lt; |w^{(t)}|^2 + 1
\end{align}\]</span> and, again by induction, <span class="math display">\[\begin{align}
  |w^{(t+1)}|^2 
  &lt; |w^{(0)}|^2 + (t+1).
\end{align}\]</span></p>
<p>Plugging <strong>(1)</strong> and <strong>(2)</strong> into <span class="math inline">\(\eqref{eq_cos}\)</span> yields: <span class="math display">\[\begin{align}
  1 \geq \cos \varphi^{(t)} &gt; 
  \frac{w^*\cdot w^{(0)} + \delta (t+1)}{\sqrt{|w^{(0)}|^2 + (t+1)}}.
\end{align}\]</span></p>
<p>The right-hand side of this equation grows as <span class="math inline">\(\sqrt{t+1}\)</span>. However, the left-hand side is bounded from above by one. Hence, only finite many update are possible before contradicting the bound which concludes the proof.</p>
<p><span class="math inline">\(\square\)</span></p>
<dl>
<dt>👀</dt>
<dd><h5 id="homework">Homework</h5>
<ol type="1">
<li>We have made several simplification, such as projection to the unit sphere, absolute separability, zero bias term, and learning rate fixed to <span class="math inline">\(\eta=1/2\)</span> to easily infer a geometric picture. However, it turns out that with slight modifications of our bounds we can readily drop all of them and obtain a similar result. Generalize our theorem above to prove: Given any affine-linearly separable and finite training data, any learning rate <span class="math inline">\(\eta&gt;0\)</span>, then the Perceptron succeeds to separate the training data after only finite many executions of the update rule. In this general setting, compute the number of updates in terms of initial weights <span class="math inline">\(w^{(0)}\)</span>, learning rate <span class="math inline">\(\eta\)</span> and margin bound <span class="math inline">\(\delta\)</span>. Is <span class="math inline">\(\delta\)</span> accessible at all?</li>
<li>The separating hyperplane is not unique. Which one does the Perceptron choose? Should the training data not be linearly separable, does the Perceptron convergence? Is the algorithm still of use in case of “almost”-linear separability of training data?</li>
</ol>
</dd>
</dl>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
