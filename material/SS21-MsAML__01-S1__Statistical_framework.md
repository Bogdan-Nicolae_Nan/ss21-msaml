# MsAML 01-S1 [[PDF](SS21-MsAML__01-S1__Statistical_framework.pdf)]

1. [Statistical framework](#statistical-framework)
   * [Classification](#classification)
   * [Regression](#regression)
   * [A challenge in statistics](#a-challenge-in-statistics)

Back to [index](index.md).


## Statistical framework

Based on the setting introduced in the module [Supervised learning setting](./SS21-MsAML__01-3__Supervised_learning_setting.md) a supervised learning algorithm $\mathcal A$, or short supervised learner, can be understood as a map of sequences of training data of variable length
\begin{align}
  s=(x^{(i)},y^{(i)})_{i=1,\ldots, N} 
\end{align}
to a hypothesis
\begin{align}
  h_s:\mathcal X\to\mathcal Y,
\end{align}
which then produces a label prediction $h_s(x)$ in the label space $\mathcal Y$ for every feature $x$ in the feature space $\mathcal X$ in a way such that

1. it performs well on the known training data $s$, i.e., has only few errors $h_s(x^{(i)})\neq y^{(i)}$;
2. and has a good chance to generalize well to unseen data samples, i.e., to also accurately predict the actual labels $y\in\mathcal Y$ for previously unseen data $x\in\mathcal X$ with the $h_s(x)$.

In the following we want to introduce a mathematical framework to capture this rather vaguely formulated goal of learning more precisely.

In particular due to the above point 2. a probabilistic setting is natural:

* Potential features and label combinations
  \begin{align}
    (x,y) \in \mathcal X\times\mathcal Y
  \end{align}
  shall be modeled by random variables $X,Y$. Throughout the script, we will try to use capital letters for random variables and lowercase letters for their values.

* For simplicity we assume:
    * the $\sigma$-algebra over $\mathcal X\times \mathcal Y$ to be of product form;
    * P to be a measure on the $\sigma$-algebra over $\mathcal X\times\mathcal Y$;
    * and if not noted otherwise, all non-explicit functions used are assumed to be measurable.

* Training data is modeled by an i.i.d. family of random variables denoted by 
  \begin{align}
    S=(X^{(i)},Y^{(i)})_{1,\ldots,N}.
  \end{align}

* As further notation, let us introduce:
    * $E$ as expectation value w.r.t. $P$.
    * $P^N$ as the product measure on $(\mathcal X\times\mathcal Y)^N$.
    * For a distribution $D$ we will write
      \begin{align}
        E_{Z\sim D}(f(Z))
      \end{align}
      to express that we take the expectation value of a function $f$ of the random
      variable $Z$ which has distribution $D$.
    * For $(Z_1,\ldots,Z_n)$ i.i.d. according to distribution $D$ we write
      \begin{align}
        (Z_1,\ldots,Z_n)\sim D^N.
      \end{align}
    * For random variables $F,G$ we often write the conditional expectation as
      \begin{align}
        E_F f(F,G) = E\left( f(F,G) \big| G \right).
      \end{align}

👀
: Recall the definition of conditional probability and expectation.

Returning to our algorithm $\mathcal A$ in this mathematical setting, we need means to evaluate the accuracy of a hypothesis $h$ predicting the labels
\begin{align}
  h(x)\in\mathcal Y
\end{align}
for given features
\begin{align}
  x\in\mathcal X
\end{align}
and actual values
\begin{align}
  y\in\mathcal Y.
\end{align}
For this purpose, we introduce the function
\begin{align}
  L:\mathcal Y\times\mathcal Y\to \mathbb R
\end{align}
with which we will evaluate the correctness of the predicted label $h(x)$ with respect to the expected actual label $y$ by means of its value
\begin{align}
  L(y, h(x)).
\end{align}
This function has many names, e.g., *loss, cost, objective, ... function*. Its purpose is to somewhat evaluate the accuracy of a prediction. Before we give concrete examples, think of it as being large if the prediction $h(x)$ has little in common with the actual label $y$ and small if it is actually pretty close or equal to $y$.

Equipped with this probabilistic framework on this loss function, we can revisit the goal of learning:

Learning goal:
: To find a hypothesis $h$ that, for a given loss function $L$, minimizes the so-called risk
  \begin{align}
    R_h := E L(Y, h(X)).
  \end{align}
  Notice that $X,Y$ are now random variables and $E$ is the expectation with
  respect to their product distribution. This expression of the risk also goes
  under the name *generalization error*.

This seems to be a sensible and simple definition but it looks more innocent than maybe meets the eye at first sight. In particular, we will constantly be faced with the following difficulty:

⚠️
: $P$, i.e., the distribution of $X,Y$, is typically unknown, which means that
  $R_h$ cannot even be computed.

All that is known to the supervised learning is the training data sample tuple
$s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}$. Hence, we will have to make good use of
statistics in order to nevertheless derive upper bounds on the risk or
generalization error, usually called *generalization bounds*, although we known
almost nothing about the distribution of $X,Y$. For this reason the field of
supervised machine learning we will be looking at is also called *statistical
learning theory*.

👀
: Please keep this point in mind until we have our first example of a
  generalization bound and revisit this thought to appreciate how it was even
  possible to derive it with so little information.

Before we dive into the generalization bounds, let us introduce some further vocabulary. One distinguishes between two general types of machine learning problems:

### Classification 

If the set of labels $\mathcal Y$ is discrete, one refers to the learning problem as *classification*. The hypothesis $h$ is called a *classifier* and the elements of $\mathcal Y$ are called *class labels*.

A typical loss function is given by
\begin{align}
  L(y,y') = 1_{y\neq y'},
\end{align}
where
\begin{align}
  1_{y\neq y'}
  =
  \begin{cases}
    1 \text{ for }{y\neq y'}\\
    0 \text{ otherwise}
  \end{cases}.
\end{align}

The risk then takes the form
\begin{align}
  R(h)
  &=
  EL(Y,h(X))\\
  &=
  E 1_{Y\neq h(X)}\\
  &=
  P(Y\neq h(X)),
\end{align}
which is called *error probability*.

In case, of $|\mathcal Y|=2$, e.g., for class labels $\mathcal Y=\{-1,1\}$ or $\mathcal Y=\{0,1\}$, one calls the classification problem *binary classification*.

Note that the framework we are introducing does not necessarily require that
there is only one actual label $y\in\mathcal Y$ for each feature sample
$x\in\mathcal X$, in other words that there is a map
\begin{align}
  c:\mathcal X\to\mathcal Y
\end{align}
such that
\begin{align}
  P(Y=y|X=x)
  &=
  \frac{P(X=x \wedge Y=y}{P(X=x)}
  \\
  &=
  1_{y=c(x)}.
\end{align}
If this is the case, $c$ is usually called a *concept* and the problem is
referred to as *deterministic*. For example, in case of noisy data
containing classification errors, this might not be true anymore, but also for
general classification problems, the existence of a concept $c$ may not be
justified. Consider, for example, the classification of the gender by means of
height for men and women. Certainly, at least in the mid ranges any large
enough training data set will show some samples which equal features but
different labels.

In general, the lower bound of the error probability is ruled by the *Bayes classifier*:
\begin{align}
  h_B(x)
  &:=
  \text{sign}\left(E(Y|X=x)\right) \\
  &=
  \text{sign}\left(\sum_{y\in\mathcal Y} y P(Y=y|X=x)\right),
\end{align}
where
\begin{align}
  \text{sign}(x)
  =
  \begin{cases}
    +1 \text{ for }x\geq 0\\
    -1 \text{ otherwise}
  \end{cases}
\end{align}

Theorem
: ##### (Bayes classifier)
  For $\mathcal Y=\{-1,+1\}$ and $L(y,y')=1_{y\neq y'}$ we have:
  \begin{align}
    \forall h\in\mathcal Y^{\mathcal X}: R(h_B)\leq R(h).
  \end{align}

**Proof:** ✍ Homework.

The theorem entails that the Bayes classifier minimizes the risk. To measure the performance of other classifiers $h$ it is therefore useful to consider the difference
\begin{align}
  R(h) - R(h_B),
\end{align}
which is called the *excess risk*.


### Regression

In the case of continuous $\mathcal Y$, one calls the learning problem
*regression*. The feature sample $x\in\mathcal X$ is referred to as *predictor*
and the value $y\in\mathcal Y$ as *response*. 

👀
: The term regression was
  originally coined by Galton as "regression towards mediocrity" to describe the
  observation that ensembles of children of comparably small and tall parents
  tend to have taller and smaller children, respectively.

A typical loss function is of the form
\begin{align}
  L(y,y') = |y-y'|^2,
\end{align}
for which the corresponding risk
\begin{align}
  R(h)=EL(Y,h(x))
\end{align}
is called *mean square error*.

👀
: A familiar regression example is least squares polynomial fitting.

One of the reasons for its popularity is its geometric
nature:

Theorem
: ##### (Geometric nature of conditional expectation) 
  Suppose $Eh(X)^2, EY^2<\infty$, then for the mean square error risk:
  \begin{align}
    R(h) 
    = 
    \underbrace{E\big|Y-E(Y|X)\big|^2}_{\text{vanishes in the deterministic case}}
    +
    \underbrace{E\big|h(X)-E(Y|X)\big|^2}_{\text{$L^2$-distance between hypothesis and $E(Y|X)$}}
  \end{align}

**Proof:** ✍ Homework.

The function $r(x):=E(Y|X=x)$ is called *regression
function* which reflect a best guest of the label
despite the noise -- recall the discussion in the
discreet setting above. The first summand in the
theorem above vanishes for a deterministic concept. The
second one vanishes for $h$ being the regression
function $r$, or in other words, $r$ is a risk
minimizer.


### A challenge in statistics

Coming back to the question of how a learning algorithm $\mathcal A$ can
attempt to minimize the risk $R(h)$ over accessible hypothesis functions $h$, we recall that the sole input information is given by the training data sequence:
\begin{align}
  s=(x^{(i)},y^{(i)})_{1,\ldots,N}.
\end{align}

As we have already observed the risk $R(h)$ itself cannot be computed, the only accessible quantity is the *empirical risk*:
\begin{align}
  \widehat R(h) \equiv \widehat R_s(h) 
  :=
  \frac{1}{N}\sum_{i=1}^N L\big(y^{(i)},h(x^{(i)})\big),
\end{align}
where we will often drop the subscript $s$ referencing the particular data
sample tuple $s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}$.  In the case of
$L(y,y')=1_{y\neq y'}$ one case this quantity *empirical error*.

An algorithm could now attempt to minimize $\widehat R(h)$ over accessible
hypothesis function $h$. Suppose a minimizer is given by $\widehat h$, which then
inherently depends on the training data $s$. One may hope that, for
sufficiently large $N$, the quantities $\widehat R(\widehat h)$ and $R(\widehat h)$
are closely related.

To substantiate this hope will be the main objective of this statistics part of the course.

Of course, a straight-forward relation is:

Theorem
: ##### (Expectation of the empirical risk)
  For any hypothesis $h$ we have $E_{S\sim P^N}\widehat R_S(h)=R(h)$.

**Proof:** ✍ Homework.

However, this is academic and we would like to understand, how the difference between $\widehat R(h)$ and $R(h)$ behave depending on $N$.

Note also that the minimization problem to find $\widehat h$ may be quite subtle.
Although for finite $|\mathcal Y|$ there is always a minimizer $\widehat
h\in\text{range}\mathcal A$ such that
\begin{align}
  \inf_{h\in\text{range}\mathcal A} \widehat R(h)= \widehat R(\widehat h),
\end{align}
since the problem reduces to a search for a discreet function
\begin{align}
  h:\{x^{(1)},\ldots, x^{(N)}\} \to \mathcal Y
\end{align}
in the range of $\mathcal A$ of which there are only finite many, finding a
minimum may yet be a very computationally heavy task.

As we will see along the way, there is often only one ingredient, that will
make this taks feasible: *Prior knowledge*. The latter can often be encoded by
means of convenient restrictions on the range of $\mathcal A$ and convenient
choices of loss function.

This ends our introduction to the statistical framework. The goal of the next
module will be to understand better the nature of the difference between
$\widehat R(\widehat h)$ and the actual quantity $R(\widehat h)$ we are interested in.

➢ Next session!
