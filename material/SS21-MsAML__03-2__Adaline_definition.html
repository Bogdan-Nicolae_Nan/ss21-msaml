<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__03-2__Adaline_definition</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-03-2-pdf">MsAML-03-2 [<a href="SS21-MsAML__03-2__Adaline_definition.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#adaline-definition">Adaline definition</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="adaline-definition">Adaline definition</h2>
<p>The main drawbacks of the Perceptron algorithm are:</p>
<ul>
<li>The update rule stops updating the model parameters <span class="math inline">\(w\)</span> and <span class="math inline">\(b\)</span> as soon as linear separability is reached, even if there might be separating hyperplanes that have more potential to generalize.</li>
<li>In case of “almost” linear separability, the update rule causes the model parameters to jump back and forth due to the remaining incorrect classifications instead of searching for an optimal hyperplane – e.g., one with the lowest number of violations.</li>
</ul>
<p>It will turn out that a minor modification allows to address these shortcomings to some extend. In particular, it will allow to specify better a notion of optimality in a way that allows to apply regular optimization theory.</p>
<p>In the Perceptron algorithm, training data feeds back information to the update rule as follows:</p>
<figure>
<img src="03/Perceptron_feedback_loop.png" alt="Illustration of the Perceptron feedback loop." /><figcaption aria-hidden="true">Illustration of the Perceptron feedback loop.</figcaption>
</figure>
<p>This illustration depicts the various steps carried out during an update. The feature signals in form of the feature vector <span class="math inline">\(x\in\mathbb R^d\)</span> are fed into the weighted sum <span class="math inline">\(w\cdot x\)</span>. The produced “signal” of the artificial neuron is then lifted by means of the bias term <span class="math inline">\(b\)</span> to result in the activation signal <span class="math inline">\(w\cdot x+b\)</span>. Afterwards, it is “cleaned” to enforce an output signal <span class="math inline">\(y\in\{-1,+1\}\)</span>. In case of a misclassification, it is the cleaned, and therefore discrete, output signal that is fed back to the update rule in order to adjust the weights in <span class="math inline">\(w\)</span> and the bias <span class="math inline">\(b\)</span>.</p>
<p>Next, we will implement a tiny adaptation of this feedback loop that will turn out to have a quite big effect:</p>
<figure>
<img src="03/Adaline_feedback_loop.png" alt="Illustration of the Adaline feedback loop." /><figcaption aria-hidden="true">Illustration of the Adaline feedback loop.</figcaption>
</figure>
<p>This architecture is called the <em>ada</em>ptive <em>line</em>ar neuron, or Adaline neuron for short. The main difference is that we will change the algorithm to continuously feed information about the activation signal back before making the signal discrete by means of the <span class="math inline">\(\text{sign}\)</span> function.</p>
<p>The activation signal will be computed from <span class="math inline">\(w\cdot x+b\)</span> by applying another function <span class="math display">\[\begin{align}
  \alpha:\mathbb R\to\mathbb R,
\end{align}\]</span> the so-called <em>activation function</em> whose value we will call <em>activation output</em>. A sufficiently regular choice in this function will be the first ingredient to apply regular optimization theory.</p>
<p>Note that the activation input <span class="math inline">\(w\cdot x+b\)</span> is unbounded, which could render a comparison to <span class="math inline">\(y^{(i)}\)</span> ill-posed as the activation input and the actual labels live on different scales. However, we could simulate the discrete <span class="math inline">\(\text{sign}\)</span> function by a smooth version of it. For example for <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span>, we could choose the so-called <em>sigmoid</em> function <span class="math display">\[\begin{align}
  \alpha(z):=\tanh(z).
\end{align}\]</span> Now <span class="math inline">\(y{(i)}\)</span> and <span class="math inline">\(h^\alpha_{(w,b)}\)</span> are again on the “same” scale, and furthermore, <span class="math inline">\(h^\alpha_{(w,b)}\)</span> is as smooth as <span class="math inline">\(\alpha\)</span>.</p>
<p>Next, let us introduce a loss function to formalize the comparison between actual labels <span class="math inline">\(y^{(i)}\)</span> and activation output <span class="math inline">\(h^\alpha_{(w,b)}\)</span>. As for <span class="math inline">\(\alpha\)</span>, we will discuss various choices in the coming modules, but for now let us assume that our label space <span class="math inline">\(\mathcal Y\)</span> comprises only the numbers <span class="math inline">\(-1\)</span> and <span class="math inline">\(+1\)</span> and start with the simple choice: <span class="math display">\[\begin{align}
  L: \mathcal Y &amp;\to\mathcal Y
  \\
  (y,y&#39;) &amp;\mapsto \frac12|y-y&#39;|^2,
\end{align}\]</span> which for training data point <span class="math inline">\((x^{(i)},y^{(i)})\)</span> equals half the square distance between the actual class label <span class="math inline">\(y^{(i)}\)</span>. The factor <span class="math inline">\(\frac12\)</span> is only kept for convenience as we will mainly be working with the corresponding derivative. This allows to define the loss between actual label <span class="math inline">\(y^{(i)}\)</span> and activation output as response to input <span class="math inline">\(x^{(i)}\)</span> by <span class="math display">\[\begin{align}
  L\left(y^{(i)},h^\alpha_{(w,b)}(x^{(i)})\right),
  \qquad
  h^\alpha_{(w,b)}(x^{(i)}) := \alpha(w\cdot x+b).
\end{align}\]</span> Furthermore, we accumulate these losses and define the empirical risk of the activation output hypothesis <span class="math inline">\(h^\alpha_{(w,b)}\)</span> as <span class="math display">\[\begin{align}
  \widehat R(h^\alpha_{(w,b)})=\frac{1}{N}\sum_{i=1}^N L\left(y^{(i)},h^\alpha_{(w,b)}(x^{(i)})\right).
\end{align}\]</span> which, to emphasize it once more, is as smooth as <span class="math inline">\(\alpha\)</span> and <span class="math inline">\(L\)</span>.</p>
<dl>
<dt>Remark.</dt>
<dd>Note however that the hypothesis used for assigning the predicted class labels is not given by the activation output <span class="math inline">\(h^\alpha_{(w,b)}\)</span> but by the “cleaned” hypothesis <span class="math display">\[\begin{align}
h_{(w,b)}(x) := \text{sign}(h^\alpha_{(w,b)}(x))
  \end{align}\]</span> that projects the activation output to the discreet label space <span class="math inline">\(\mathcal Y\)</span>. Hence, although closely related, the empirical risk of the activation output <span class="math inline">\(h^\alpha_{(w,b)}\)</span> will differ from the empirical risk of the classifier <span class="math inline">\(h_{(w,b)}\)</span> <span class="math display">\[\begin{align}
\widehat R(h_{(w,b)})=\frac{1}{N}\sum_{i=1}^N L\left(y^{(i)},h_{(w,b)}(x^{(i)})\right),
  \end{align}\]</span> and likewise, of the error probability <span class="math display">\[\begin{align}
\widehat{\text{Err}}(h_{(w,b)})=\frac{1}{N}\sum_{i=1}^N 1_{y^{(i)}\neq h_{(w,b)}(x^{(i)})}.
  \end{align}\]</span>
</dd>
</dl>
<p>It is the goal, of course, to minimize the empirical risk <span class="math inline">\(\widehat R(h_{(w,b)})\)</span>. However, due to the <span class="math inline">\(\text{sign}\)</span> function, the latter is not continuous, not to mention differentiable, no matter how smooth <span class="math inline">\(\alpha\)</span> or <span class="math inline">\(L\)</span> was chosen; and the same holds for the empirical error. This makes it impossible to apply regular optimization methods. Therefore, instead of attempting to minimize the empirical risk directly we will focus on the closely related empirical risk of the activation output for sufficiently smooth <span class="math inline">\(\alpha\)</span> and <span class="math inline">\(L\)</span>.</p>
<p>The introduction of <span class="math inline">\(\alpha\)</span> and <span class="math inline">\(L\)</span> will help us to quip the model with a mathematically precise notion of priori knowledge about:</p>
<ul>
<li>How close the model is to make an accurate classification of the training data;</li>
<li>and how optimal a potential hypothesis is.</li>
</ul>
<p>Especially, for the latter, <span class="math inline">\(L\)</span> has to be chosen with much care.</p>
<p>To keep our notation short, let us introduce the abbreviation <span class="math display">\[\begin{align}
  \widehat L(w,b) = \widehat R(h^\alpha_{(w,b)})
\end{align}\]</span> and let us for a moment assume that the model parameters <span class="math inline">\(w\)</span> and <span class="math inline">\(b\)</span> are already sufficiently close to a local minimum of <span class="math inline">\(\widehat R(h^\alpha_{(w,b)})\)</span>.</p>
<p>How should the model parameters <span class="math inline">\(w\)</span> and <span class="math inline">\(b\)</span> be updated <span class="math display">\[\begin{align}
  w &amp; \leftarrowtail w^\text{new} := w + \delta w \\
  b &amp; \leftarrowtail b^\text{new} := b + \delta b
\end{align}\]</span> to get even closer to the minimum, i.e., such that <span class="math display">\[\begin{align}
  \widehat L(w^\text{new})\leq \widehat L(w)
  \tag{min}
  \label{eq_min}
\end{align}\]</span> is fulfilled?</p>
<p>Let us consider a heuristic argument and employ the notation <span class="math inline">\(\overline w=(w,b)\)</span>: <span class="math display">\[\begin{align}
  \widehat L(\overline w^\text{new}) - \widehat L(\overline w)
  &amp;=
  \widehat L(\overline w+\delta \overline w) - \widehat L(\overline w)
  \\
  &amp;=
  \frac{\partial \widehat L(\overline w)}{\partial \overline w} \delta\overline w
  +
  O_{|\delta \overline w|\to 0}(|\delta \overline w|^2),
\end{align}\]</span> where <span class="math inline">\(\frac{\partial}{\partial\overline w}\)</span> denotes the gradient in <span class="math inline">\(\mathbb R^{d+1}\)</span> with respect to <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in\mathbb R\)</span>. Hence, choosing <span class="math display">\[\begin{align}
  \delta\overline w = -\eta \frac{\partial \widehat L(\overline w)}{\partial \overline w}
  \tag{negative gradient}
  \label{eq_negative_gradient}
\end{align}\]</span> for a learning rate <span class="math inline">\(\eta&gt;0\)</span>, results in <span class="math display">\[\begin{align}
  \widehat L(\overline w^\text{new}) - \widehat L(\overline w)
  &amp;=
  \widehat L(\overline w+\delta \overline w) - \widehat L(\overline w)
  \\
  &amp;=
  -\eta
  \left|\frac{\partial \widehat L(\overline w)}{\partial \overline w} w\right|^2
  +
  O_{\eta \to 0}(\eta^2).
\end{align}\]</span> Note how our choice <span class="math inline">\(\eqref{eq_negative_gradient}\)</span> in taking the negative gradient of <span class="math inline">\(\widehat L\)</span> at <span class="math inline">\(\overline w\)</span> fixes the sign of the linear order term. In case the higher order corrections, whose modulus can be estimated by means of the Taylor remainder, in our case, involving the Hesse matrix of <span class="math inline">\(\widehat L(\overline w)\)</span>, does not outweigh this negativity of first order term, the new parameters <span class="math inline">\(\overline w+\delta\overline w\)</span> brought us even closer to the minimum, i.e., <span class="math inline">\(\eqref{eq_min}\)</span>. This is the core idea behind a procedure that is referred to as <em>gradient descent</em>, owing its name to <span class="math inline">\(\eqref{eq_negative_gradient}\)</span>.</p>
<dl>
<dt>👀</dt>
<dd>Is there a bound on the higher orders which is accessible in applications?
</dd>
</dl>
<p>Of course, this heuristic discussion only scratches the surface of search policies to find a minimum of <span class="math inline">\(\widehat L\)</span> in regular optimization theory. Under which conditions do we converge? And if we do, to which local minimum? And will this local minimum be a global one? What to do to not get stuck in local minima that still feature a high value of <span class="math inline">\(\widehat L\)</span>? And there are many more sophisticated algorithms, like the Newton’s method you might recall from your analysis lectures to mention a simple one. Nevertheless, it will turn out that, for application, the gradient descent will perform reasonably well without relying on second order derivatives that are very expensive to compute once the model parameters become large.</p>
<p>In summary, we may formalize the Adaline update rule as follows:</p>
<p><strong>Update rule:</strong> The component-wise Adaline update rule is given by <span class="math display">\[\begin{align}
  w &amp; \leftarrowtail w^\text{new}_j := w_j -\eta \frac{\partial \widehat L(w,b)}{\partial w_j} \\
  b &amp; \leftarrowtail b^\text{new} := b -  \eta \frac{\partial \widehat L(w,b)}{\partial b}
\end{align}\]</span> for components <span class="math inline">\(j=1,\ldots,d\)</span>.</p>
<p>Furthermore, we specify the first version of the Adaline algorithm by:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="adaline">(Adaline)</h5>
The realization (as it is actually a random variable when defined as below) of the Adaline algorithm is a map <span class="math display">\[\begin{align}
\mathcal A:\cup_{N\in\mathbb N} (\mathbb R^d,\{-1,1\})^N\to \mathcal H
  \end{align}\]</span> which is specified by means of the computation steps below:
</dd>
</dl>
<p><strong>Input for <span class="math inline">\(\mathcal A\)</span>:</strong> Training data <span class="math display">\[\begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N}.
\end{align}\]</span></p>
<p><strong>Step 1:</strong> Initialize <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in \mathbb R\)</span> with random coefficients which defines the activation output <span class="math display">\[\begin{align}
  h^\alpha_{(w,b)}(x) := \alpha(w\cdot x +b).
\end{align}\]</span> and hypothesis <span class="math display">\[\begin{align}
  h_{(w,b)}(x) := \text{sign}(w\cdot x +b).
\end{align}\]</span></p>
<p><strong>Step 2:</strong> Until a maximum total number of repetitions is reached:</p>
<ol type="1">
<li>Compute the empirical activation loss <span class="math inline">\(\widehat L(w,b)\)</span>.</li>
<li>Conduct the <strong>Update rule</strong> above to infer the parameter updates: <span class="math display">\[\begin{align}
 w &amp; \leftarrowtail w^\text{new}\\
 b &amp; \leftarrowtail b^\text{new}
    \end{align}\]</span></li>
</ol>
<p>and return to <strong>Step 2</strong>.</p>
<p><strong>Output of <span class="math inline">\(\mathcal A\)</span>:</strong> The adjusted parameters <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in\mathbb R\)</span> and therefore a “hopefully” better hypothesis <span class="math display">\[\begin{align}
  h_{(w,b)} \in\mathcal H.
\end{align}\]</span></p>
<p>Each iteration in the above algorithm is usually called a <em>training epoch</em>. In the next module we will compare the Perceptron and Adaline algorithms a bit closer and discuss some useful variations.</p>
<p>➢ Next session!</p>
<dl>
<dt>👀 Homework.</dt>
<dd><ol type="1">
<li>Prove that, even for linearly separable data, the Adaline algorithm for constant <span class="math inline">\(\eta&gt;0\)</span> may not converge, no many how many epochs we allow. This will even typically be the case. Argue why the Adaline may nevertheless behave better than the Perceptron in view of our initial motivation above.</li>
<li>Consider a linear activation function <span class="math inline">\(\alpha(z)=z\)</span> and the square loss <span class="math inline">\(L(y,y&#39;)=\frac12|y-y&#39;|2\)</span> as above. Given training data <span class="math inline">\(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\)</span>, does a minimum of <span class="math inline">\(\widehat L(w,b)\)</span> exist? Can we enforce convergence by allowing the Adaline algorithm, e.g., to decrease <span class="math inline">\(\eta\)</span>, after each epoch of training? Discuss the trade-off between speed of convergence and accuracy for smaller <span class="math inline">\(\eta\)</span>.</li>
</ol>
</dd>
</dl>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
