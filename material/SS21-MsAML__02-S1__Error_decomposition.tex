% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-02-s1-pdf}{%
\section{\texorpdfstring{MsAML-02-S1
{[}\href{SS21-MsAML__02-S1__Error_decomposition.pdf}{PDF}{]}}{MsAML-02-S1 {[}PDF{]}}}\label{msaml-02-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{error-decomposition-and-generalization-bounds}{Error
  decomposition and generalization bounds}

  \begin{itemize}
  \tightlist
  \item
    \protect\hyperlink{trade-off-considerations}{Trade-off
    considerations}
  \end{itemize}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{error-decomposition-and-generalization-bounds}{%
\subsection{Error decomposition and generalization
bounds}\label{error-decomposition-and-generalization-bounds}}

In the last module, we have introduced a statistical framework in which
we can grasp the learning performance more precisely. However, we also
realized that its study may become rather tricky in non-academic
settings in which all we have at our expense is the empirical data.
Hence, the actual risk \(R\) is unaccessible directly.

Instead of being able to minimize the risk \(R(h)\) over a certain space
of hypotheses \(h\in\mathcal H\) directly we can only access the
empirical \(\widehat R_s(h)\) average of it given our realization of
training data
\(s\in\cup_{N\in\mathbb N}(\mathcal X\times\mathcal Y)^N\). Hence, at
best we may try to minimize the empirical risk \(\widehat R_s(h)\) over
a hypotheses \(h\in\mathcal H\) that are accessible the supervised
learning, i.e., the particular algorithm \(\mathcal A\), and hope that,
for large samples sizes \(N=|s|\) the obtained minimum of the empirical
risk \(\widehat R(\widehat h)\) for some minimizer \(\widehat h\) is
with larger probability not too far from the actual infimum of \(R\).

This approach is called \emph{empirical risk minimization}.

To better understand the challenges in such an approach, it makes sense
to identify the different sources for differences in
\(\widehat R(\widehat h)\) and \(R^*\), which goes by the name of
\emph{error decomposition}.

For our purposes in this module, let \(\mathcal H\) be the largest set
of sensible hypothesis candidates, i.e., \begin{align}
  \mathcal H := \left\{h\in \mathcal Y^\mathcal{X} \,|\, h \text{ measurable}\right\},
\end{align} and let us define \begin{align}
   R^* := \inf_{h \in\mathcal H}R(h),
\end{align} which is called the \emph{Bayes risk}. A hypothesis \(h^*\)
with \(R^*=R(h^*)\) is then called a Bayes classifier, which however may
not need to exist.

Suppose further, the supervised learner, an algorithm \(\mathcal A\),
has a range \begin{align}
   \mathcal F:=\text{range}\mathcal A \subseteq \mathcal H,
\end{align} say, due to the particularities and constraints of its
implementation. We may then define the optimal risk on this subspace by
\begin{align}
   R_\mathcal{F} := \inf_{h\in\mathcal F}R(h).
\end{align}

Let \(\widehat h_s=\mathcal A(s)\) denote a hypothesis learn by
algorithm \(\mathcal A\) by means of inspecting training \(s\). We may
then go ahead and decompose the original difference of interest for a
hypothesis \(h\in\mathcal F\): \begin{align}
   R(h)-R^*
   =
   & \phantom{-}\left(R(h)-R(\widehat h)\right)
   \tag{Optimization Error}
   \\
   & +\left(R(\widehat h)-R_\mathcal{F}\right)
   \tag{Estimation Error}
   \\
   & +\left(R_\mathcal{F}-R^*\right)
   \tag{Approximation Error}
\end{align}

This decomposition is useful because:

\begin{itemize}
\tightlist
\item
  \(\widehat h\) is the only entity that depends on the empirical sample
  of training data and is usually the only accessible object;
\item
  \(R_\mathcal F\) depends only the abilities of the algorithm
  \(\mathcal A\);
\item
  \(R^*\) depends only on the learning task.
\end{itemize}

Therefore:

\begin{itemize}
\tightlist
\item
  the optimization error compares a hypothesis \(h\) against the learned
  hypothesis \(\widehat h\) in terms of their actual risk;
\item
  the estimation error reflects how well the actual risk of the learned
  hypothesis \(\widehat h\) performs when compared against the
  theoretically optimal performance of the algorithm \(\mathcal A\),
  namely the minimizer of the risk over \(\mathcal F\);
\item
  the approximation error only depends on the learning task and the
  available hypotheses in \(\mathcal F\) the algorithm \(\mathcal A\)
  can reach.
\end{itemize}

Clearly, when given a learning task and an algorithm, the approximation
error is what it is. Nevertheless, the hope formulated above, i.e., that
the right-hand side decreases when increasing the sample size of
training data \(N\), due to the optimization and estimation error, and
we will spend a large part of this course trying to justify this
intuition in various settings.

Hence, let us assume that, for given training data \(s\), the algorithm
\(\mathcal A\) always succeeds in the empirical minimization and learns
a hypothesis \(\widehat h_s\) such that \begin{align}
  \forall h\in\mathcal F: \, \widehat R_s(\widehat h_s)\leq \widehat R(h).
\end{align} To a gain more control the remaining difference of interest,
observe for any \(h\in\mathcal F\): \begin{align}
  & R(\widehat h) - R(h) 
  \\
  &= 
    R(\widehat h)            - \widehat R(\widehat h)
                 + \underbrace{\widehat R(\widehat h) - \widehat R(h)}_{\leq 0 \text{ as $\widehat h$ minimizes $\widehat R$}}
        + \widehat R(h) - R(h)
  \\
  &\leq
  R(\widehat h)  - \widehat R(\widehat h)
        + \widehat R(h) - R(h)
          \\
  &\leq 
  |R(\widehat h)  - \widehat R(\widehat h)|
  + |\widehat R(h) - R(h)|
  \\
  &\leq
  2\sup_{h'\in\mathcal F}|\widehat R(h')-R(h')|
  \tag{Generalization error}
  \label{eq_generalization_error}
\end{align}

As this inequality holds for all \(h\in\mathcal F\), we can also take
the infimum\\
over \(h\in\mathcal F\) on the left-hand side to find \begin{align}
  & R(\widehat h)-R_\mathcal{F} 
  \leq
  2\sup_{h'\in\mathcal F}|\widehat R(h')-R(h')|.
\end{align}

Hence, in order to substantiate our hope it suffices to provide bounds
for the right-hand side of \(\eqref{eq_generalization_error}\) in terms
of sample size \(N\). As these bounds provide information on how well a
hypothesis \(h\) generalizes from observed training data to unseen test
data, they a usually referred as \emph{generalization bounds}.

\hypertarget{trade-off-considerations}{%
\subsubsection{Trade-off
considerations}\label{trade-off-considerations}}

Neglecting the optimization error for a moment typically reveals the
following trade-off:

\begin{itemize}
\tightlist
\item
  A smaller approximation error may require a more complex hypotheses
  space \(\mathcal F\) that contains hypotheses which are better adapted
  to learning task at hand;
\item
  The estimation error typically grows with the complexity of
  \(\mathcal F\) as there are more an more peculiarities to consider
  when choosing a hypothesis.
\end{itemize}

To better understand such a trade-off let us regard a very similar
setting as in our discussion of Theorem (Geometric nature of conditional
expectation) in the
\href{SS21-MsAML__01-S1__Statistical_framework.pdf}{Statistical
framwork} module with the following ingredients:

\begin{itemize}
\tightlist
\item
  The random variables \((X,Y)\sim P\) taking values in
  \(\mathcal X\times\mathcal Y\);
\item
  Training data \(S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}\) as a random
  variable taking values in \((\mathcal X,\mathcal Y)^N\) and being
  distributed according to \(P^N\).
\item
  The pair-wise independence of
  \((X,Y), (X^{(1)},Y^{(1)}), \ldots, (X^{(N)},Y^{(N)})\);
\item
  A hypothesis \begin{align}
     h_S := \mathcal A(S)
  \end{align} that is learned by an algorithm \(\mathcal A\) on the
  basis of training data \(S\).
\item
  The assumption that that \(E_{(X,Y)\sim P} h_S(X)^2, EY^2<\infty\) so
  that also the mean square error risk
  \(R(h_S):=E_{(X,Y)\sim P}|Y-h(X)|^2\) to be well-defined;
\item
  The assumption \begin{align}
    E_{S\sim P^N} R(h_S)<\infty;
  \end{align}
\item
  The expected prediction denoted by: \begin{align}
     \overline h(\cdot) = E_{S\sim P^N} h_S(\cdot).
  \end{align} Note the dependence of \(h_S\) on \(S\) and the
  independence of \(\overline h\) of \(S\).
\end{itemize}

\begin{description}
\item[Theorem] ~ 
\hypertarget{noise-bias-variance-decomposition}{%
\subparagraph{(Noise-bias-variance
decomposition)}\label{noise-bias-variance-decomposition}}

In the above setting we have \begin{align}
 E_{S\sim P^N} R(h_S) &\\
 = & \underbrace{E_{(X,Y)\sim P} |Y-E(Y|X)|^2}_\text{expected noise}
 \\
 & + \underbrace{E_{(X,Y)\sim P} |\overline h(X)-E(Y|X)|^2}_\text{expected bias}
 \\
 & + \underbrace{E_{(X,Y)\sim P} E_{S\sim P^N}|h_S(X)-\overline h(X)|^2}_\text{expected variance}
  \end{align}
\end{description}

\textbf{Proof:} Recall Theorem (Geometric nature of conditional
expectation) for the
\href{SS21-MsAML__01-S1__Statistical_framework.pdf}{Statistical
framwork} module. For a realization of training data
\(s\in(\mathcal X\times\mathcal Y)^N\) it states: \begin{align}
  R(h_s) = E|Y-E(Y|X)|^2 + E|h_s(X)-E(Y|X)|^2.
\end{align} Replacing the realization \(s\) with a random variable \(S\)
and taking the expectation with respect to \(S\) yields: \begin{align}
  E_{S\sim P^N} R(h_S) = & E_{S\sim P^N} E_{(X,Y)\sim P} |Y-E(Y|X)|^2
  \tag{S1}
  \label{eq_first_summand}
  \\
  & + E_{S\sim P^N} E_{(X,Y)\sim P} |h_S(X)-E(Y|X)|^2
  \tag{S2}
  \label{eq_second_summand}
\end{align} The first summand is already to our liking. The second
summand reads: \begin{align}
  \eqref{eq_second_summand} = 
  & E_{S\sim P^N} E_{(X,Y)\sim P} |h_S(X)-\overline h(X)+\overline h(X)-E(Y|X)|^2 
  \\
  = & \phantom{+} E_{S\sim P^N} E_{(X,Y)\sim P} |h_S(X)-\overline h(X)|^2 
  \tag{S2.1}
  \label{eq_second_first_summand}
  \\
  & + E_{S\sim P^N} E_{(X,Y)\sim P} |\overline h(X)-E(Y|X)|^2 
  \tag{S2.2}
  \label{eq_second_second_summand}
  \\
  & + 2\underbrace{E_{S\sim P^N} E_{(X,Y)\sim P} 
                  \left(h_S(X)-\overline h(X)\right)
                  \left(\overline h(X)-E(Y|X)\right)}_{
                      =E_{(X,Y)\sim P}\left(\overline h(X)-E(Y|X)\right)
                      E_{S\sim P^N}\left(h_S(X)-\overline h(X)]\right)
                  }.
\end{align} Note that by definition
\(E_{S\sim P^N}\left(h_S(X)-\overline h(X)\right)=0\). Hence, only
\(\eqref{eq_first_summand},\eqref{eq_second_first_summand},\eqref{eq_second_second_summand}\)
remain in the original expression of the expectation of the actual risk.
Furthermore, we observe that in \(\eqref{eq_first_summand}\) and
\(\eqref{eq_second_second_summand}\) we may drop the expectation over
\(S\sim P^N\) and in \(\eqref{eq_second_first_summand}\) we may
interchange the expectation values over \(S\sim P^N\) and
\((X,Y)\sim P\) by Fubini, which concludes the proof.

\(\square\)

The noise term was already discussed in module
\href{SS21-MsAML__01-S1__Statistical_framework.pdf}{Statistical
framwork}. It vanished for a deterministic concept. In addition we have:

\begin{itemize}
\tightlist
\item
  The expected bias that reflects an inherent preference of the
  algorithm \(\mathcal A\) in picking a hypothesis \(h_S\) on the basis
  of training data \(S\), which may stem from the implementation of
  algorithm \(\mathcal A\), in particular, the richness in complexity of
  the hypotheses \(\mathcal F\) accessible to algorithm \(\mathcal A\).
\item
  The expected variance reflects how much the computed \(h_S\) depends
  on the training data sample \(S\), or put a bit more negatively, how
  much the algorithm \(\mathcal A\) adapted the hypothesis \(h_S\) to
  the particularities of the inspected training data at hand, which
  might however not be generally relevant features.
\end{itemize}

To illustrate this further, consider the example of the well-known
least-square polynomial fitting algorithm from your analysis classes
that can be used to approximate a function \begin{align}
  f:\mathbb R\to\mathbb R
\end{align} given a set of potentially noisy sample points, i.e., random
variables \begin{align}
  S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N} \in (\mathbb R\times\mathbb R)^N,
\end{align} by means of a polynomial \(h:\mathbb R\to\mathbb R\) of
degree \(\leq d\in\mathbb N_0\) that minimizes the least square distance
to the sample points at the positions \(X^{(i)}\), i.e., in our new
jargon, the empirical risk: \begin{align}
  \widehat R_S(h) = \frac{1}{N}\sum_{i=1}^N L(Y^{(i)},h(X^{(i)})), \qquad L(y,y')=|y-y'|^2.
\end{align}

In order to decrease the expected bias of the algorithm \(\mathcal A\)
we may enrich the range of \(\mathcal A\), i.e., \(\mathcal F\), by
increasing the degree \(d\). This allows the corresponding hypotheses
\(h\in\mathcal F\) to adapt more easily to the training data \(s\). A
typical plot of two such hypotheses could look as follows:

\begin{figure}
\centering
\includegraphics{02/High_polynomial_degree.png}
\caption{Two realizations of hypotheses for a high polynomial degree and
two realizations of training data samples.}
\end{figure}

Note that in the illustration, due to the small amount of noise, the red
and blue realizations of samples points almost do not differ. Thanks to
the high degree in the polynomial both correspondingly learned
hypotheses manage easily to hit all the sample points and the expected
bias is correspondingly small. However, the illustration already hints
at the potentially large variance in the hypotheses even though the
noise is so small -- the polynomials may even flip their asymptotic
behavior due to a tiny change in one sample point!

Going to the other extreme by drastically decreasing the degree of the
polynomial, one has the opposite effect:

\begin{figure}
\centering
\includegraphics{02/Low_polynomial_degree.png}
\caption{Two realizations of hypotheses for a low polynomial degree and
two realizations of training data samples.}
\end{figure}

Now the expected variance is reduced as we are left with straight lines
in the vicinity of the sample points. This obviously comes at the cost
of a large expected bias.

The takeaway message is that, if algorithms have a too large space of
hypotheses with respect to the complexity of the learning task, there is
the danger that they adapt too well to the features found in the
inspected training data but generalize poorly to unseen data that might
in general not express them. This phenomenon we have already mentioned
by the name \emph{overfitting}. In our framework, we can give it the
following formal meaning:

\begin{description}
\item[Definition] ~ 
\hypertarget{overfitting}{%
\subparagraph{(Overfitting)}\label{overfitting}}

Let \(\mathcal A\) be an algorithm, a hypothesis
\(h\in\text{range}\mathcal A\) is said to be overfitted if:
\begin{align}
\exists h'\in\text{range}\mathcal A: \widehat R(h) < \widehat R(h') \quad \wedge \quad R(h)>R(h').
  \end{align}
\end{description}

As we have already discussed, the tendency of an algorithm to choose an
overly complex hypothesis is neither easy to monitor nor to control as
\(R\) cannot be computed without the actual distribution of the training
data.

A first attempt to monitor the learning behavior of an algorithm is to
implement a cross validation which we have briefly discussed in module
\href{SS21-MsAML__02-1__Linear_classification.pdf}{Linear classification
and Perceptron definition}: Repeatedly divide the available data
randomly into a test data set \(T\) and training data set \(S\) and
evaluate the empirical risk of the learned hypothesis \(h_S\) on both
\(S\) and \(T\) denoted by \(\widehat R_S(h_S)\) and
\(\widehat R_T(h_S)\), respectively. Should \(\widehat R_S(h_S)\) tend
to be much lower than \(\widehat R_T(h_S)\), it is a good indication for
overfitting, or in other words that the empirical risk is likely too
optimistic with respect to the actual risk. Such monitoring efforts
obviously depend on the availability of a large enough the fairness of
the picked sample of data with respect to the distribution \(P\).

During the course we will review several techniques that help to study
an optimal trade-off between bias and variation. However, before going
into that, we first need to understand better how, without knowing the
actual distribution of the training data \(P\), we even have a chance to
derive inequalities such as the above mentioned generalization bounds
\(\eqref{eq_generalization_error}\) in terms of the sample size \(N\).

➢ Next session!

\end{document}
