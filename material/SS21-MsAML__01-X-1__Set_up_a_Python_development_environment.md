# Exercise 01-X1: Set up a Python development environment [[PDF](SS21-MsAML__01-X1__Set_up_a_Python_development_environment.pdf)]

Depending on the platform macOS, Linux/Unix, Windows, etc. you are using there
are several options to set up a Python development environment. There are
several ingredients of a Python development environment:

**Essential ingredients:**

* A Python distribution including Python interpreter and the standard library
  of Python packages, each of which provides certain Python functionality;
* Any kind of text editor to edit a `your_script.py` text file to be
  interpreted by Python typically using the console command;
    ```
    >> python your_script.py
    ```
* The Python debugger `pdb` that is actually a Python package included in the
  distribution.

**Convenient ingredients:**

* An integrated development environment such as
  [PyCharm](https://www.jetbrains.com/de-de/pycharm/), [VS Code](https://code.visualstudio.com/), etc. 
  that allows to edit, run, debug Python scripts, inspect the console, features
  code completion and linting, helps to organize projects, employ version
  control, etc. without having to leave the application;

**Something light-weight in between:**

* [Jupyter Notebook](https://jupyter.org) which is a web-based
  interactive development environment for [Jupyter Notebooks](https://jupyter.org) 
  that allow to create mixed documents of live code, equations, visualizations
  and narrative text.

All of the above require to install a Python distribution. For this there are
two approaches:

* *Worry-free bundle:* Anaconda by Continuum Analytics is a free bundle
  of a Python distribution that, among other things, ships with the essential
  data science Python packages. 

  * [Download page](https://www.anaconda.com/distribution/)
  * [Install instructions](https://docs.anaconda.com/anaconda/install/)
  * [Quick start guide](https://docs.anaconda.com/anaconda/user-guide/getting-started/)
  
  After a successful install, further Python packages can be installed using
  the console command

        conda install package_name_to_install

  and update outdated Python packages
        
        conda update package_name_to_update

* *Hands-on method:* While the above Anaconda bundle provides an easy install
  procedure and does not require much maintenance it is also quite rigid. For a
  slightly more difficult but more minimal and configurable installation you
  can install the Python distribution manually. The platform dependent
  installation procedures are well explained on the 
  [official Python site](https://www.python.org) and the latest version can be
  obtained here:

  * https://www.python.org/downloads/
  
  Platforms such as macOS and Unix/Linux come with a pre-installed Python
  distribution, however, maybe with only the old Python 2 version in which case
  a Python 3 installation is necessary. Check the version by

  ```
  >> python --version
  ```

  Sometimes a parallel installation of a Python 3 interpreter carries the name
  `python3`, so also try:

  ```
  >> python3 --version
  ```

  Python 3 can also be conveniently installed using common software package
  manager. For example:

  * [Homebrew](https://brew.sh/) for macOS, once installed using the command
    ```
    >> /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    ```

    allows to install the Python distribution simply by

    ```
    brew install python
    ```
  
  * Similarly on Linux distributions such as Ubuntu you can use the standard
    software package manager to install Python as follows 

    ```
    sudo apt-get install python3 
    ```

  After a successful install, missing Python packages can be installed using
  the console. For example, some standard package we may used can be installed
  as follows:

        pip install numpy scipy matplotlib pandas jupyter

  and they can be updated with
        
        pip install --upgrade package_name_to_update

As a started kit, especially, if you have not yet used an integrated
development environment, I recommend you to install the Python distribution
with Anaconda and take a look at Jupyter Notebook or Lab that is included. If
you are working with VS Code the python extension will also allow you to load
Jupyter notebook and work with them interactively as explained
[here](https://code.visualstudio.com/docs/python/jupyter-support) and
[here](https://code.visualstudio.com/docs/python/jupyter-support-py).

It has a focus on data science and suits our purposes well as it allows for a
quick and dynamic *call and response* type of programming mixing code with
documentation that is even nicely rendered on GitLab -- though it is a terrible
format for version tracking software. Once installed, it can be launched by:

    >> jupyter notebook

to edit a single Jupyter notebook or

    >> jupyter lab

for the more sophisticated environment.

With an installation approach of your choice, set up a Python development
environment and write your first data science equivalent of a hello world
program:

* Hello world of data science [Python Script](./01/hello_world_of_data_science.py), [Python Notebook](./01/hello_world_of_data_science.ipynb)]
